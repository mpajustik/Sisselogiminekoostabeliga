﻿namespace LogingBox
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SalvestanButton = new System.Windows.Forms.Button();
            this.EesnimiLabel = new System.Windows.Forms.Label();
            this.PerekonnanimiLabel = new System.Windows.Forms.Label();
            this.AmetikohtLabel = new System.Windows.Forms.Label();
            this.OYLabel = new System.Windows.Forms.Label();
            this.RaamatupidajaCB1 = new System.Windows.Forms.CheckBox();
            this.AktiivneCB = new System.Windows.Forms.CheckBox();
            this.EesnimiBox = new System.Windows.Forms.TextBox();
            this.PerekonnanimiBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.IsikukoodLable = new System.Windows.Forms.Label();
            this.IsikukoodBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.AmetikohtBox = new System.Windows.Forms.TextBox();
            this.YlemusBox = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // SalvestanButton
            // 
            this.SalvestanButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SalvestanButton.Location = new System.Drawing.Point(207, 454);
            this.SalvestanButton.Name = "SalvestanButton";
            this.SalvestanButton.Size = new System.Drawing.Size(256, 50);
            this.SalvestanButton.TabIndex = 0;
            this.SalvestanButton.Text = "Salvestan Andmebaasi";
            this.SalvestanButton.UseVisualStyleBackColor = true;
            this.SalvestanButton.Click += new System.EventHandler(this.SalvestanButton_Click);
            // 
            // EesnimiLabel
            // 
            this.EesnimiLabel.AutoSize = true;
            this.EesnimiLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.EesnimiLabel.Location = new System.Drawing.Point(46, 168);
            this.EesnimiLabel.Name = "EesnimiLabel";
            this.EesnimiLabel.Size = new System.Drawing.Size(74, 20);
            this.EesnimiLabel.TabIndex = 1;
            this.EesnimiLabel.Text = "Eesnimi:";
            this.EesnimiLabel.Click += new System.EventHandler(this.EesnimiLabel_Click);
            // 
            // PerekonnanimiLabel
            // 
            this.PerekonnanimiLabel.AutoSize = true;
            this.PerekonnanimiLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PerekonnanimiLabel.Location = new System.Drawing.Point(46, 222);
            this.PerekonnanimiLabel.Name = "PerekonnanimiLabel";
            this.PerekonnanimiLabel.Size = new System.Drawing.Size(124, 20);
            this.PerekonnanimiLabel.TabIndex = 2;
            this.PerekonnanimiLabel.Text = "Perekonnanimi:";
            // 
            // AmetikohtLabel
            // 
            this.AmetikohtLabel.AutoSize = true;
            this.AmetikohtLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AmetikohtLabel.Location = new System.Drawing.Point(46, 276);
            this.AmetikohtLabel.Name = "AmetikohtLabel";
            this.AmetikohtLabel.Size = new System.Drawing.Size(88, 20);
            this.AmetikohtLabel.TabIndex = 3;
            this.AmetikohtLabel.Text = "Ametikoht:";
            this.AmetikohtLabel.Click += new System.EventHandler(this.AmetikohtLabel_Click);
            // 
            // OYLabel
            // 
            this.OYLabel.AutoSize = true;
            this.OYLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OYLabel.Location = new System.Drawing.Point(46, 334);
            this.OYLabel.Name = "OYLabel";
            this.OYLabel.Size = new System.Drawing.Size(127, 20);
            this.OYLabel.TabIndex = 4;
            this.OYLabel.Text = "Otsene ülemus:";
            // 
            // RaamatupidajaCB1
            // 
            this.RaamatupidajaCB1.AutoSize = true;
            this.RaamatupidajaCB1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RaamatupidajaCB1.Location = new System.Drawing.Point(299, 375);
            this.RaamatupidajaCB1.Name = "RaamatupidajaCB1";
            this.RaamatupidajaCB1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RaamatupidajaCB1.Size = new System.Drawing.Size(18, 17);
            this.RaamatupidajaCB1.TabIndex = 5;
            this.RaamatupidajaCB1.UseVisualStyleBackColor = true;
            this.RaamatupidajaCB1.CheckedChanged += new System.EventHandler(this.True);
            // 
            // AktiivneCB
            // 
            this.AktiivneCB.AutoSize = true;
            this.AktiivneCB.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AktiivneCB.Location = new System.Drawing.Point(299, 413);
            this.AktiivneCB.Name = "AktiivneCB";
            this.AktiivneCB.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.AktiivneCB.Size = new System.Drawing.Size(18, 17);
            this.AktiivneCB.TabIndex = 6;
            this.AktiivneCB.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.AktiivneCB.UseVisualStyleBackColor = true;
            this.AktiivneCB.CheckedChanged += new System.EventHandler(this.True);
            // 
            // EesnimiBox
            // 
            this.EesnimiBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.EesnimiBox.Location = new System.Drawing.Point(207, 158);
            this.EesnimiBox.Name = "EesnimiBox";
            this.EesnimiBox.Size = new System.Drawing.Size(256, 30);
            this.EesnimiBox.TabIndex = 2;
            // 
            // PerekonnanimiBox
            // 
            this.PerekonnanimiBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PerekonnanimiBox.Location = new System.Drawing.Point(207, 212);
            this.PerekonnanimiBox.Name = "PerekonnanimiBox";
            this.PerekonnanimiBox.Size = new System.Drawing.Size(256, 30);
            this.PerekonnanimiBox.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(46, 375);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(180, 20);
            this.label1.TabIndex = 11;
            this.label1.Text = "Kas on raamatupidaja?";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(46, 410);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(209, 20);
            this.label2.TabIndex = 12;
            this.label2.Text = "Kas on ettevõttes aktiivne?";
            // 
            // IsikukoodLable
            // 
            this.IsikukoodLable.AutoSize = true;
            this.IsikukoodLable.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IsikukoodLable.Location = new System.Drawing.Point(51, 114);
            this.IsikukoodLable.Name = "IsikukoodLable";
            this.IsikukoodLable.Size = new System.Drawing.Size(83, 20);
            this.IsikukoodLable.TabIndex = 13;
            this.IsikukoodLable.Text = "Isikukood:";
            // 
            // IsikukoodBox
            // 
            this.IsikukoodBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IsikukoodBox.Location = new System.Drawing.Point(207, 104);
            this.IsikukoodBox.Name = "IsikukoodBox";
            this.IsikukoodBox.Size = new System.Drawing.Size(256, 30);
            this.IsikukoodBox.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(49, 24);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(418, 36);
            this.label3.TabIndex = 14;
            this.label3.Text = "Andmebaasi kasutaja loomine:";
            // 
            // AmetikohtBox
            // 
            this.AmetikohtBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AmetikohtBox.Location = new System.Drawing.Point(207, 266);
            this.AmetikohtBox.Name = "AmetikohtBox";
            this.AmetikohtBox.Size = new System.Drawing.Size(256, 30);
            this.AmetikohtBox.TabIndex = 4;
            // 
            // YlemusBox
            // 
            this.YlemusBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.YlemusBox.Location = new System.Drawing.Point(207, 324);
            this.YlemusBox.Name = "YlemusBox";
            this.YlemusBox.Size = new System.Drawing.Size(256, 30);
            this.YlemusBox.TabIndex = 15;
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1192, 516);
            this.Controls.Add(this.YlemusBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.IsikukoodBox);
            this.Controls.Add(this.IsikukoodLable);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.AmetikohtBox);
            this.Controls.Add(this.PerekonnanimiBox);
            this.Controls.Add(this.EesnimiBox);
            this.Controls.Add(this.AktiivneCB);
            this.Controls.Add(this.RaamatupidajaCB1);
            this.Controls.Add(this.OYLabel);
            this.Controls.Add(this.AmetikohtLabel);
            this.Controls.Add(this.PerekonnanimiLabel);
            this.Controls.Add(this.EesnimiLabel);
            this.Controls.Add(this.SalvestanButton);
            this.Name = "Main";
            this.Text = "Main";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button SalvestanButton;
        private System.Windows.Forms.Label EesnimiLabel;
        private System.Windows.Forms.Label PerekonnanimiLabel;
        private System.Windows.Forms.Label AmetikohtLabel;
        private System.Windows.Forms.Label OYLabel;
        private System.Windows.Forms.CheckBox RaamatupidajaCB1;
        private System.Windows.Forms.CheckBox AktiivneCB;
        private System.Windows.Forms.TextBox EesnimiBox;
        private System.Windows.Forms.TextBox PerekonnanimiBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label IsikukoodLable;
        private System.Windows.Forms.TextBox IsikukoodBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox AmetikohtBox;
        private System.Windows.Forms.TextBox YlemusBox;
    }
}