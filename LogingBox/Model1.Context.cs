﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace LogingBox
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class ReisikuludeHaldamiseSysteemVMEntities : DbContext
    {
        public ReisikuludeHaldamiseSysteemVMEntities()
            : base("name=ReisikuludeHaldamiseSysteemVMEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<AruandeOlek> AruandeOleks { get; set; }
        public virtual DbSet<KuluAruanne> KuluAruannes { get; set; }
        public virtual DbSet<Kulukirjed> Kulukirjeds { get; set; }
        public virtual DbSet<Kululiigid> Kululiigids { get; set; }
        public virtual DbSet<SisselogimiseTabel> SisselogimiseTabels { get; set; }
        public virtual DbSet<Töötaja> Töötaja { get; set; }
    }
}
